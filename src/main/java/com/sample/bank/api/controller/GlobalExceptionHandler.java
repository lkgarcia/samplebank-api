package com.sample.bank.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by kevin on 13/2/18.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason="Unexpected error")
    @ExceptionHandler(Exception.class)
    public void handleException(Exception ex){
        logger.error("Unexpected error occurred.", ex);
    }
}
