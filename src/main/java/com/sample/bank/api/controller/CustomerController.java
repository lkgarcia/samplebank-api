package com.sample.bank.api.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.sample.bank.api.domain.entity.Customer;
import com.sample.bank.api.model.Views;
import com.sample.bank.api.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kevin on 13/2/18.
 */

@RestController
public class CustomerController {

    @Autowired
    CustomerService service;


    @JsonView(Views.CustomerOnly.class)
    @RequestMapping(value = "/api/v1/customer/{id}", method = RequestMethod.GET)
    public Customer getCustomer(@PathVariable("id") String id) {
        return service.getCustomer(id);
    }

    @JsonView(Views.CustomerAndAccounts.class)
    @RequestMapping(value = "/api/v1/customer/{id}/accounts", method = RequestMethod.GET)
    public Customer getCustomerAccounts(@PathVariable("id") String id) {
        return service.getCustomer(id);
    }
}
