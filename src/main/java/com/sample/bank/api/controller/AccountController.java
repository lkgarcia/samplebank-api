package com.sample.bank.api.controller;

import com.sample.bank.api.domain.entity.Account;
import com.sample.bank.api.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kevin on 13/2/18.
 */

@RestController
public class AccountController {

    @Autowired
    AccountService service;


    @RequestMapping(value = "/api/v1/account/{id}", method = RequestMethod.GET)
    public Account getAccount(@PathVariable("id") String id) {
        return service.getAccount(id);
    }
}
