package com.sample.bank.api.model;

/**
 * Created by kevin on 13/2/18.
 */
public class Views {
    public static class CustomerOnly {}

    public static class CustomerAndAccounts extends CustomerOnly {}
}
