package com.sample.bank.api.service;

import com.sample.bank.api.domain.entity.Account;
import com.sample.bank.api.domain.repository.AccountRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by kevin on 13/2/18.
 */

@Service
public class AccountService {

    @Autowired
    private AccountRepository repository;


    public Account getAccount(String id) {
        System.out.println("Parameter (id):" + id);
        System.out.println("findAll:" + repository.findAll());
        return  repository.findOne(id);
        // return  null;
    }
}
