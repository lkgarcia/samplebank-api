package com.sample.bank.api.service;

import com.sample.bank.api.domain.entity.Customer;
import com.sample.bank.api.domain.repository.CustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by kevin on 13/2/18.
 */

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository repository;


    public Customer getCustomer(String id) {
        return  repository.findOne(id);
    }
}
