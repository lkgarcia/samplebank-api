package com.sample.bank.api.domain.repository;

import com.sample.bank.api.domain.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by kevin on 13/2/18.
 */
public interface AccountRepository extends JpaRepository<Account, String> {

}
