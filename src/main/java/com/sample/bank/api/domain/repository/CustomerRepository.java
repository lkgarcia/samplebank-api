package com.sample.bank.api.domain.repository;

import com.sample.bank.api.domain.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by kevin on 13/2/18.
 */
public interface CustomerRepository extends JpaRepository<Customer, String> {
}
