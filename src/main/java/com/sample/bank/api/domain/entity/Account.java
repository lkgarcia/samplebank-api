package com.sample.bank.api.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.sample.bank.api.model.Views;

import javax.persistence.Entity;
import javax.persistence.Id;


/**
 * Created by kevin on 13/2/18.
 */
// @Document
@Entity
public class Account {

    @Id
    @JsonView(Views.CustomerAndAccounts.class)
    String id;

    @JsonView(Views.CustomerAndAccounts.class)
    String currency;

    @JsonView(Views.CustomerAndAccounts.class)
    double balance;

    @JsonIgnore
    String createDate;

    @JsonIgnore
    String updateDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
